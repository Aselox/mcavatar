# name: sharplay
# about: sharplaynetwork plugin for discourse
# version: 0.1
# authors: Boris D'Amato

after_initialize do
  User.class_eval do
    def avatar_template
      "http://minepic.org/avatar/{size}/" + username
    end
  end
end